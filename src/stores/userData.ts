import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/type/User'

export const useUserDataStore = defineStore('userData', () => {
  const defaultUser: User = {
    id: -1,
    username: '',
    password: '',
    fullName: ''
  }
  const users: User[] = ([
    {
      id: 1,
      username: 'admin',
      password: 'pass1',
      fullName: 'admin1'
    },
    {
      id: 2,
      username: 'user',
      password: 'pass2',
      fullName: 'admin1'
    },
    {
      id: 3,
      username: 'Ant',
      password: 'pass3',
      fullName: 'user2'
    },
    {
      id: 4,
      username: 'Bird',
      password: 'pass3',
      fullName: 'user3'
    }
  ])

  return { defaultUser, users }
})