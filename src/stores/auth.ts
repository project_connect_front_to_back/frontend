import router from '@/router'
import { inject, ref, type Ref } from 'vue'
import { defineStore } from 'pinia'
import { useUserDataStore } from './userData'
import type { User } from '@/type/User'

export const useAuthStore = defineStore('auth', () => {
  const userDataStore = useUserDataStore()
  const showDrawer = ref<boolean>(false)

  const currentUser = ref<User>(userDataStore.defaultUser)

  function isAuthenticated() {
    return currentUser.value.id > -1
  }

  function checkUser(username: string, password: string): boolean {
    for (const user of userDataStore.users)
      if (user.username === username && user.password === password) {
        currentUser.value = user
        router.push('/home')
        showDrawer.value = true
        console.log(currentUser.value)
        return true
      }
    console.log('Authentication failed')
    return false
  }
  function logout(): void {
    currentUser.value = userDataStore.defaultUser
    router.push('/login')
    showDrawer.value = false
  }

  return {
    currentUser,
    router,
    showDrawer,
    isAuthenticated,
    checkUser,logout
  }
})
