import { ref, computed, provide } from 'vue'
import { defineStore } from 'pinia'
//mport type { ReceiptItem } from '@/type/ReceiptItemIng'
import type { ReceiptIngStore } from '@/type/ReceiptIngStore'
import type { ReceiptItemIng } from '@/type/ReceiptItemIng'
import type { Ingredient } from '@/type/Ingredient'
import { useProductStore } from './product'
import { isTemplateExpression } from 'typescript'

export const useReceiptIngStore = defineStore('receipt', () => {
  //const authStore = useAuthStore()
  //const memberStore = useMemberStore()
  const productStore = useProductStore()
  const receiptIngDialog = ref(false)
  const receiptIng = ref<ReceiptIngStore>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    totalAmount: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash'
  })
  const receiptItemsIng = ref<ReceiptItemIng[]>([])

  function addReceiptItemIng(Ingredient: Ingredient) {
    const index = receiptItemsIng.value.findIndex((item) => item.ingredient?.id === Ingredient.id)
    // console.log(receiptIng)
    if (index >= 0) {
      receiptItemsIng.value[index].amount++
      // calReceipt()
      console.log('clickkk1111' + receiptItemsIng.value[index])
      calReceipt()
      return
    } else {
      console.log('clickkk2222' + receiptItemsIng.value[index])
      const newReceiptIng: ReceiptItemIng = {
        id: -1,
        name: Ingredient.name,
        price: Ingredient.price,
        amount: 1,
        unit: Ingredient.unit,
        ingreId: Ingredient.id,
        ingredient: Ingredient
      }
      receiptItemsIng.value.push(newReceiptIng)
      calReceipt()
    }
  }

  function removeReceiptItem(receiptItem: ReceiptItemIng) {
    const index = receiptItemsIng.value.findIndex((item) => item === receiptItem)
    receiptItemsIng.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItemIng) {
    item.amount++
    calReceipt()
    console.log('+++')
  }

  function dec(item: ReceiptItemIng) {
    if (item.amount === 1) {
      removeReceiptItem(item)
      console.log('delete')
    }
    item.amount--
    calReceipt()
    console.log('---')
  }

  function calReceipt() {
    let totalBefore = 0
    let amount = 0
    for (const item of receiptItemsIng.value) {
      totalBefore = totalBefore + (item.price * item.amount)
      amount = amount + item.amount
    }
    receiptIng.value.totalBefore = totalBefore
    receiptIng.value.totalAmount = amount
    receiptIng.value.total = totalBefore
  }

  function showReceiptIngDialog() {
    //receiptIng.value.receiptItemsIng = receiptItemsIng.value
    receiptIngDialog.value = true
  }

  function clear() {
    receiptItemsIng.value = []
    receiptIng.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      totalAmount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash'
      //userId: authStore.currentUser.id,
      //user: authStore.currentUser,
      //memberId: 0
    }
    //memberStore.clear()
  }
  function save(Amount: number) {
    receiptIng.value.receivedAmount = Amount;
    productStore.cashDialog = false
    // productStore.clear()
}
  // provide('useReceiptIng', {
  //   receiptIng,
  //   receiptItemsIng,
  //   addReceiptItemIng
  // })

  return {
    receiptIng,
    receiptItemsIng,
    receiptIngDialog,
    addReceiptItemIng,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    showReceiptIngDialog,
    clear,
    save
  }
})
