import router from '@/router'
import { nextTick, onMounted, ref } from 'vue'
import { defineStore } from 'pinia'
import { useHistory } from '@/stores/history'
import http from '@/services/http'
import type { Ingredient } from '@/type/check'
import IngredientSer from '@/services/ingredient'
export const useCheck = defineStore('checkStore', () => {
  const historyStore = useHistory()

  const requiredFields = ['name', 'inStock', 'Maximum', 'Unit']
  const selectedIngredient = ref<Ingredient>({
    id: -1,
    name: '',
    inStock: 0,
    Maximum: 0,
    Unit: '',
    createdDate: new Date()
  })

  const purchase = ref(false)
  const form = ref(false)
  const loading = ref(false)
  const dialogDelete = ref(false)
  const formRef = ref(null)
  let editedIndex = -1
  let lastId = 4
  const initilIngredient: Ingredient = {
    createdDate: new Date(),
    id: -1,
    name: '',
    inStock: 1,
    Maximum: 10,
    Unit: ''
  }
  const editedIngredient = ref<Ingredient>(JSON.parse(JSON.stringify(initilIngredient)))
  const dialog = ref(false)
  const ingredients = ref<Ingredient[]>([])
  const headers = [
    {
      title: 'Id',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Instock',
      key: 'inStock',
      value: 'inStock'
    },
    {
      title: 'Maximum ',
      key: 'Maximum',
      value: 'Maximum'
    },
    {
      title: 'Unit',
      key: 'Unit',
      value: 'Unit'
    },
    { title: 'Actions', key: 'actions', sortable: false }
  ]
  const unsavedChanges = ref(false)
  async function onSubmit() {
    if (validateForm()) {
      // Proceed with submission logic
    } else {
      // Handle validation error, e.g., show a message to the user
      alert('Please fill in all required fields')
    }
  }

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedIngredient.value = Object.assign({}, initilIngredient)
      lastId += 1
    })
  }

  function finishShowStock() {
    // ถ้าคุณต้องการส่งข้อมูล ingredients ไปยังเส้นทาง checkStock
    const route = '/checkStock'

    // ใช้ Vue Router เพื่อนำทางและส่งข้อมูลเป็นพารามิเตอร์คิวรี
    router.push({ path: route, query: { ingredients: JSON.stringify(ingredients.value) } })
  }

  function naviInShowCheckStock() {
    router.push('/checkStock')
  }

  function editItem(item: Ingredient) {
    editedIndex = ingredients.value.indexOf(item)
    editedIngredient.value = Object.assign({}, item)
    dialog.value = true
    unsavedChanges.value = true
  }
  // function เกี่ยวกับ เริ่มต้นข้อมูล
  onMounted(() => {
    initialize()
  })
  async function initialize() {
    const res = await http.get('/ingredient')
    ingredients.value = res.data
  }
  // function เกี่ยวกับ Savr
  async function Save() {
    if (validateForm()) {
      if (editedIngredient.value.id < 0) {
        await saveIngredient(editedIngredient.value)
      } else {
        await updateIngredient(editedIngredient.value)
      }

      closeDialog()
    } else {
      alert('Please fill in all required fields')
    }
  }

  async function updateIngredient(ingredient: Ingredient) {
    const res = await IngredientSer.update(ingredient)
    editedIngredient.value.createdDate = new Date()
    const changes = {
      ...editedIngredient.value,
      createdDate: editedIngredient.value.createdDate.getTime()
    }
    historyStore.addToHistory({
      name: editedIngredient.value.name,
      id: editedIngredient.value.id,
      inStock: editedIngredient.value.inStock,
      Maximum: editedIngredient.value.Maximum,
      Unit: editedIngredient.value.Unit,
      editedDate: new Date(),
      changes
    })
    Object.assign(ingredients.value[editedIndex], editedIngredient.value)

    await initialize()
  }

  async function saveIngredient(ingredient: Ingredient) {
    const res = await IngredientSer.addNew(ingredient)
    lastId += 1 // เพิ่มค่า lastId ทุกครั้งที่มีการเพิ่มข้อมูลใหม่
    editedIngredient.value.id = lastId

    const changes = {
      ...editedIngredient.value,
      createdDate: new Date().getTime()
    }

    historyStore.addToHistory({
      name: editedIngredient.value.name,
      id: editedIngredient.value.id,
      inStock: editedIngredient.value.inStock,
      Maximum: editedIngredient.value.Maximum,
      Unit: editedIngredient.value.Unit,
      editedDate: new Date(),
      changes
    })

    await initialize() // เรียกใหม่เพื่อโหลดข้อมูลใหม่หลังจากบันทึก
  }

  // function เกี่ยวกับ การเตือนของฟอร์ม
  function validateForm(): boolean {
    for (const field of requiredFields) {
      const fieldValue = editedIngredient.value[field]

      if (!fieldValue || fieldValue === undefined || fieldValue === null) {
        console.error(`Invalid value for ${field}.`)
        return false
      }

      if (typeof fieldValue === 'string' && fieldValue.trim() === '') {
        console.error(`${field} is required`)
        return false
      }
    }
    return true
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedIngredient.value = Object.assign({}, initilIngredient)
      lastId + 1
    })
  }

  //function เกี่ยวกับการลบ
  function deleteItem(item: Ingredient) {
    editedIndex = ingredients.value.indexOf(item)
    editedIngredient.value = Object.assign({}, item)
    dialogDelete.value = true
  }

  function deleteItemConfirm() {
    deleteIngredient(editedIngredient.value)
    closeDelete()
  }
  async function deleteIngredient(user: Ingredient) {
    const res = await IngredientSer.remove(user)
    await initialize()
  }

  // function เกี่ยวกับ ปุ่มโชว์ใบซื้อสิ้นค้า
  function Showpurchase() {
    dialog.value = true
  }

  return {
    dialogDelete,
    editedIndex,
    lastId,
    initilIngredient,
    editedIngredient,
    dialog,
    ingredients,
    headers,
    onSubmit,
    closeDelete,
    finishShowStock,
    naviInShowCheckStock,
    editItem,
    deleteItem,
    initialize,
    Save,
    closeDialog,
    onMounted,
    deleteItemConfirm,
    form,
    loading,
    formRef,
    purchase,
    Showpurchase,
    selectedIngredient
  }
})
