import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/type/Member'

export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        {id: 1, name: 'Jason Smith',tel: '0871234567'},
        {id: 2, name: 'John Conner' ,tel: '0863486264' }
    ])
    const statusMember = ref(false)
    const currentMember = ref<Member | null>()
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0) {
            currentMember.value = null
        }
        currentMember.value = members.value[index]
        statusMember.value = true

    }
    function clearCurrentMember() {
        currentMember.value = null
        
    }
    
    return {
        members, currentMember,statusMember,
        searchMember, clearCurrentMember
    }
})
