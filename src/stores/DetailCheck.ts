import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { DetailCheckStocks } from '@/type/DetailCheckStocks';


export const useDetailCheckStore = defineStore('DetailCheck', () => {
    const DetailCheckStock = ref<DetailCheckStocks[]>([
        { id: 1, Date: '01-01-2024', ProductCode: '1001', Product: 'ผงชาเขียว', 'Price': 145, Min: 5, Balance: 4, Unit: 'pcs.' },
        { id: 2, Date: '01-01-2024', ProductCode: '1002', Product: 'ผงโกโก้', 'Price': 145, Min: 5, Balance: 4, Unit: 'pcs.' },
        { id: 3, Date: '01-01-2024', ProductCode: '1003', Product: 'น้ำตาล', 'Price': 189, Min: 5, Balance: 4, Unit: 'pcs.' },
        { id: 4, Date: '01-01-2024', ProductCode: '1004', Product: 'นมข้น', 'Price': 129, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 5, Date: '01-01-2024', ProductCode: '1005', Product: 'น้ำผึ้ง', 'Price': 200, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 6, Date: '01-01-2024', ProductCode: '1006', Product: 'น้ำส้ม', 'Price': 74, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 7, Date: '01-01-2024', ProductCode: '1007', Product: 'ผงชาเเดง', 'Price': 145, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 8, Date: '01-01-2024', ProductCode: '1008', Product: 'ผงกาแฟ', 'Price': 145, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 9, Date: '01-01-2024', ProductCode: '1009', Product: 'นมสด', 'Price': 72, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 10,Date: '01-01-2024', ProductCode: '1010', Product: 'น้ำเชื่อม', 'Price': 145, Min: 5, Balance: 1, Unit: 'pcs.' },

        { id: 1, Date: '07-01-2024', ProductCode: '1001', Product: 'ผงชาเขียว', 'Price': 145, Min: 5, Balance: 4, Unit: 'pcs.' },
        { id: 2, Date: '07-01-2024', ProductCode: '1002', Product: 'ผงโกโก้', 'Price': 145, Min: 5, Balance: 4, Unit: 'pcs.' },
        { id: 3, Date: '07-01-2024', ProductCode: '1003', Product: 'น้ำตาล', 'Price': 189, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 4, Date: '07-01-2024', ProductCode: '1004', Product: 'นมข้น', 'Price': 129, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 5, Date: '07-01-2024', ProductCode: '1005', Product: 'น้ำผึ้ง', 'Price': 200, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 6, Date: '07-01-2024', ProductCode: '1006', Product: 'น้ำส้ม', 'Price': 74, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 7, Date: '07-01-2024', ProductCode: '1007', Product: 'ผงชาเเดง', 'Price': 145, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 8, Date: '07-01-2024', ProductCode: '1008', Product: 'ผงกาแฟ', 'Price': 145, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 9, Date: '07-01-2024', ProductCode: '1009', Product: 'นมสด', 'Price': 72, Min: 5, Balance: 0, Unit: 'pcs.' },
        { id: 10,Date: '07-01-2024', ProductCode: '1010', Product: 'น้ำเชื่อม', 'Price': 145, Min: 5, Balance: 0, Unit: 'pcs.' },
        
        { id: 1, Date: '14-01-2024', ProductCode: '1001', Product: 'ผงชาเขียว', 'Price': 145, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 2, Date: '14-01-2024', ProductCode: '1002', Product: 'ผงโกโก้', 'Price': 145, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 3, Date: '14-01-2024', ProductCode: '1003', Product: 'น้ำตาล', 'Price': 189, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 4, Date: '14-01-2024', ProductCode: '1004', Product: 'นมข้น', 'Price': 129, Min: 5, Balance: 3, Unit: 'pcs.' },
        { id: 5, Date: '14-01-2024', ProductCode: '1005', Product: 'น้ำผึ้ง', 'Price': 200, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 6, Date: '14-01-2024', ProductCode: '1006', Product: 'น้ำส้ม', 'Price': 74, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 7, Date: '14-01-2024', ProductCode: '1007', Product: 'ผงชาเเดง', 'Price': 145, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 8, Date: '14-01-2024', ProductCode: '1008', Product: 'ผงกาแฟ', 'Price': 145, Min: 5, Balance: 0, Unit: 'pcs.' },
        { id: 9, Date: '14-01-2024', ProductCode: '1009', Product: 'นมสด', 'Price': 72, Min: 5, Balance: 0, Unit: 'pcs.' },
        { id: 10,Date: '14-01-2024', ProductCode: '1010', Product: 'น้ำเชื่อม', 'Price': 145, Min: 5, Balance: 0, Unit: 'pcs.' },

        { id: 1, Date: '22-01-2024', ProductCode: '1001', Product: 'ผงชาเขียว', 'Price': 145, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 2, Date: '22-01-2024', ProductCode: '1002', Product: 'ผงโกโก้', 'Price': 145, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 3, Date: '22-01-2024', ProductCode: '1003', Product: 'น้ำตาล', 'Price': 189, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 4, Date: '22-01-2024', ProductCode: '1004', Product: 'นมข้น', 'Price': 129, Min: 5, Balance: 2, Unit: 'pcs.' },
        { id: 5, Date: '22-01-2024', ProductCode: '1005', Product: 'น้ำผึ้ง', 'Price': 200, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 6, Date: '22-01-2024', ProductCode: '1006', Product: 'น้ำส้ม', 'Price': 74, Min: 5, Balance: 1, Unit: 'pcs.' },
        { id: 7, Date: '22-01-2024', ProductCode: '1007', Product: 'ผงชาเเดง', 'Price': 145, Min: 5, Balance: 0, Unit: 'pcs.' },
        { id: 8, Date: '22-01-2024', ProductCode: '1008', Product: 'ผงกาแฟ', 'Price': 145, Min: 5, Balance: 0, Unit: 'pcs.' },
        { id: 9, Date: '22-01-2024', ProductCode: '1009', Product: 'นมสด', 'Price': 72, Min: 5, Balance: 0, Unit: 'pcs.' },
        { id: 10,Date: '22-01-2024', ProductCode: '1010', Product: 'น้ำเชื่อม', 'Price': 145, Min: 5, Balance: 0, Unit: 'pcs.' },
      

        
      ]);
      
      const headers = [
        {
          title: 'Id',
          key: 'id',
          value: 'id',
          
        },
        {
          title: 'Date',
          key: 'Date',
          value: 'Date',
        },
        {
          title: 'ProductCode',
          key: 'ProductCode',
          value: 'ProductCode',
        },
        {
          title: 'Product',
          key: 'Product',
          value: 'Product',
        },
        {
          title: 'Price(Baht)',
          key: 'Price',
          value: 'Price',
        },
        {
          title: 'Min',
          key: 'Min',
          value: 'Min',
        },
        {
          title: 'Balance',
          key: 'Balance',
          value: 'Balance',
        },
        {
          title: 'Unit',
          key: 'Unit',
          value: 'Unit',
        },
      ];
      
      const dialog = ref(false);
      
      const nextId = computed(() => {
        const ids = DetailCheckStock.value.map(item => item.id);
        return Math.max(...ids) + 1;
      });
      
      const form = ref({
        editedStock: {
          id: nextId.value,
          date: '',
          productcode: '',
          product: '',
          'price(baht)': 0,
          min: 0,
          balance: 0,
          unit: '',
        },
        loading: false,
      });
      
      
      const search = ref('');
      const filteredDetailCheckStock = ref([...DetailCheckStock.value]);

      watch(
        search,
        (newValue) => {
          // If the search query is empty, show all data
          if (newValue === '') {
            filteredDetailCheckStock.value = [...DetailCheckStock.value];
          } else {
            // Filter the data based on the search query
            filteredDetailCheckStock.value = DetailCheckStock.value.filter((item) => {
              // Customize this condition based on your requirements
              return (
                item.Date.toLowerCase().includes(newValue.toLowerCase())
              );
            });
          }
        }
      );
      
  return { DetailCheckStock, filteredDetailCheckStock, search,form,dialog,headers }
})
