import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/type/Product'
import type { ReceiptItem } from '@/type/ReceiptItem'
import type { ReceiptPOS } from '@/type/ReceiptPOS'
import { useMemberStore } from './member'

export const useProductStore = defineStore('product', () => {
  const receiptDialog = ref(false)
  const memberDialog = ref(false)
  const searchDialog = ref(false)
  const cashDialog = ref(false)
  const prompayDialog = ref(false)
  const receiptItems = ref<ReceiptItem[]>([])
  const memberStore = useMemberStore()
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    console.log(receiptItems)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }
  function deleteReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      deleteReceiptItem(item)
    } else {
      item.unit--
      calReceipt()
    }
  }
  
  function showReceiptDialog() {
    receiptDialog.value = true
    
  }

  function showSearchDialog() {
    searchDialog.value = true
    
  }

  function showCashDialog() {
    cashDialog.value = true
    
  }
  function showPrompayDialog() {
    prompayDialog.value = true
    
  }

  function showmemberDialog() {
    memberDialog.value = true
  }

  function save(Amount: number) {
    receiptPos.value.receivedAmount = Amount;
    receiptPos.value.change = Amount-(receiptPos.value.total)
    cashDialog.value = false
    
    // productStore.clear()
}
  
  
  function searchItem(Word: string) {
    const sanitizedQuery = Word.toLowerCase()
    const searchResults = receiptItems.value
      .filter((receiptItem) => receiptItem.product?.name.toLowerCase().includes(sanitizedQuery))
      .map((receiptItem) => receiptItem.product!)
  }

  function clear() {
    receiptItems.value = []
    receiptPos.value.totalAmount = 0
    receiptPos.value.receivedAmount = 0
    receiptPos.value.totalBefore = 0
    receiptPos.value.total = 0
    receiptPos.value.change =0
    receiptPos.value.memberDiscount  = 0
    receiptPos.value.member = undefined
    receiptPos.value.memberId = -1
    memberStore.statusMember = false
  }

  function calReceipt() {
    let totalBefore = 0
    let amount = 0
    let discount = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + (item.price * item.unit)
      amount = amount+item.unit
      
    }

    receiptPos.value.totalAmount = amount
    discount = totalBefore/10
    
    receiptPos.value.totalBefore = totalBefore

    if (memberStore.statusMember) {   
      receiptPos.value.total = totalBefore - discount
      receiptPos.value.memberDiscount = discount
    } else {
      receiptPos.value.total = totalBefore
    }
  }

  const receiptPos = ref<ReceiptPOS>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    totalAmount: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    //userId: authStore.currentUser.id,
    //user: authStore.currentUser,
    memberId: 0
  })

  

  return {
    receiptItems,
    receiptDialog,
    searchDialog,
    receiptPos,
    cashDialog,
    prompayDialog,
    memberDialog,
    showmemberDialog,
    calReceipt,
    addReceiptItem,
    deleteReceiptItem,
    inc,
    dec,
    searchItem,
    showReceiptDialog,
    showSearchDialog,
    clear,
    showCashDialog,
    showPrompayDialog,
    save
  }
})
