import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import  type { Ingredient} from '@/type/Ingredient.ts'

export const useIngredientStore = defineStore('ingredient', () => {
    const ingredients = ref<Ingredient[]>([
        { id: 1, name: 'ผงชาเขียว', Amount: 1, unit: 'กิโลกรัม',price: 50},
        { id: 2, name: 'ผงโกโก้', Amount: 1, unit: 'กิโลกรัม',price: 50},
        { id: 3, name: 'น้ำตาล', Amount: 1, unit: 'กิโลกรัม',price: 50},
        { id: 4, name: 'นมข้น', Amount: 1, unit: 'กิโลกรัม',price: 50},
        { id: 5, name: 'ครีมเทียม', Amount: 1, unit: 'กิโลกรัม', price: 80 },
        { id: 6, name: 'น้ำเชื่อม', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 7, name: 'วิปครีม', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 8, name: 'ไซรัป', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 9, name: 'น้ำแข็ง', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 10, name: 'แก้ว', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 11, name: 'หลอด', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 12, name: 'เส้นสปสเก็ตตี้', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 13, name: 'ผงกาแฟ', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 14, name: 'ไข่มุก', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 15, name: 'ส้ม', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 16, name: 'บลูเบอร์รี่', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 17, name: 'ช็อคโกแลต', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 18, name: 'สตอเบอร์รี่', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 19, name: 'มะพร้าว', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 20, name: 'แป้ง', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 21, name: 'มะละกอ', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 22, name: 'ข้าวหอมมะลิ', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 23, name: 'ข้าวเหนียว', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 24, name: 'กุ้งสด', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 25, name: 'ไข่ไก่', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 26, name: 'มะเขือเทศ', Amount: 1, unit: 'กิโลกรัม', price: 40 },
        { id: 27, name: 'ต้นหอม', Amount: 1, unit: 'กิโลกรัม', price: 40 }
      ])

  return { ingredients }
})
