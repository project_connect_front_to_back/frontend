import type { Ingredient } from "./Ingredient" 

type ReceiptItemIng = {
    id: number
    name: string
    price: number
    amount: number
    unit: string
    ingreId: number
    ingredient?: Ingredient
}

export{ type ReceiptItemIng}