type Ingredient = {
    id: number
    name: string
    Amount: number
    unit: string
    price: number
}

export { type Ingredient}
