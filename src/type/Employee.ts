type Employee = {
    id: number;
    name: string;
    position: string;
    Tel: string;
  }
  export type { Employee }