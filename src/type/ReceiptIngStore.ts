import type { Ingredient } from "./Ingredient";

type ReceiptIngStore = {
    id: number;
    createdDate: Date;
    totalBefore: number;
    memberDiscount: number;
    total: number;
    totalAmount: number,
    receivedAmount: number;
    change: number;
    paymentType: string;
    //userId: number;
    // user?: User;
    // memberId: number;
    // member?: Member;
    ingredient?: Ingredient[];
}

export type{ ReceiptIngStore }