//import type { Member } from "./Member";
import type { Member } from "./Member";
import type { ReceiptItem } from "./ReceiptItem";
//mport type { User } from "./User";

type ReceiptPOS = {
    id: number;
    createdDate: Date;
    totalBefore: number;
    memberDiscount: number;
    total: number;
    totalAmount: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    //userId: number;
    //user?: User;
    memberId: number;
    member?: Member;
    receiptItem?: ReceiptItem[];
}

export type{ ReceiptPOS }