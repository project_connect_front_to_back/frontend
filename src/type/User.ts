interface User {
    id: number,
    username: string,
    password: string,
    fullName: string
}
export type { User }